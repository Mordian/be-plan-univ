package com.graphhopper.android;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Path;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.GraphHopper;
import com.graphhopper.PathWrapper;
import com.graphhopper.util.Constants;
import com.graphhopper.util.Parameters;
import com.graphhopper.util.PointList;
import com.graphhopper.util.StopWatch;

import org.mapsforge.core.graphics.Color;
import org.mapsforge.core.graphics.GraphicFactory;
import org.mapsforge.core.graphics.Paint;
import org.mapsforge.core.graphics.Style;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.map.android.graphics.AndroidGraphicFactory;
import org.mapsforge.map.android.view.MapView;
import org.mapsforge.map.layer.overlay.Polyline;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

/**
 * Class which computes paths and draws the results on the map
 */
public class WayComputer {
    /**
     * DEBUG TAG
     */
    private static final String TAG = "WayComputer";

    //Map
    private File graphFolder;
    private MapView mapView;
    private Polyline pathLayer;

    //Graph
    private GraphHopper hopper;
    private volatile boolean graphLoaded = false;
    private String currentArea;
    private Context context;

    /**
     * Build an object to compute itinerary
     * @param context the activity's context
     * @param currentArea the map's name without the extension
     * @param mapView the view which contains the map
     */
    public WayComputer(Context context, String currentArea,MapView mapView){
        this.context = context;
        this.currentArea = currentArea;
        this.mapView = mapView;
        graphFolder = new File(context.getApplicationInfo().dataDir+"/mapUPS-gh");
        String[] listFiles;
        try{
            //get the folders of the graph
            listFiles = context.getAssets().list("mapUPS-gh");
        }catch(Exception e){
            throw new RuntimeException(e);
        }
        Util.replaceAssetToCache(context,graphFolder,listFiles);
        loadGraphStorage();
    }

    /**
     * Loads the graph before using it
     */
    public void loadGraphStorage() {
        //the loading is done in background by an other thread
        new GHAsyncTask<Void, Void, Path>() {
            protected Path saveDoInBackground(Void... v) throws Exception {
                GraphHopper tmpHopp = new GraphHopper().forMobile();
                tmpHopp.load(new File(context.getApplicationInfo().dataDir,currentArea).getAbsolutePath()+"-gh");
                Log.i("","found graph " + tmpHopp.getGraphHopperStorage().toString() + ", nodes:" + tmpHopp.getGraphHopperStorage().getNodes());
                hopper = tmpHopp;
                return null;
            }

            //Verifies that the graph loading succeded
            protected void onPostExecute(Path o) {
                if (hasError()) {
                    //logUser("An error happened while creating graph:"+ getErrorMessage());
                } else {
                    graphLoaded = true;
                    //logUser("Finished loading graph. Long press to define where to start and end the route.");
                }
            }
        }.execute();
    }

    /**
     * Computes the shortest path and draws the result on the map
     * @param fromLat latitude of the starting point
     * @param fromLon longitude of the starting point
     * @param toLat latitude of the ending point
     * @param toLon longitude of the ending point
     */
    public void calcPath(final double fromLat, final double fromLon,
                         final double toLat, final double toLon) {

        Log.i("compute","calculating path ...");
        new AsyncTask<Void, Void, PathWrapper>() {
            float time;

            protected PathWrapper doInBackground(Void... v) {
                StopWatch sw = new StopWatch().start();
                GHRequest req = new GHRequest(fromLat, fromLon, toLat, toLon).
                        setAlgorithm(Parameters.Algorithms.DIJKSTRA_BI);
                req.getHints().
                        put(Parameters.Routing.INSTRUCTIONS, "false");
                GHResponse resp = hopper.route(req);
                time = sw.stop().getSeconds();
                return resp.getBest();
            }

            protected void onPostExecute(PathWrapper resp) {
                if (!resp.hasErrors()) {
                    Log.i("","from:" + fromLat + "," + fromLon + " to:" + toLat + ","
                            + toLon + " found path with distance:" + resp.getDistance()
                            / 1000f + ", nodes:" + resp.getPoints().getSize() + ", time:"
                            + time + " " + resp.getDebugInfo());
                    logUser("Durée estimée:" + resp.getTime() / 60000f+" minutes");
                    //remove the old path of the map
                    if(pathLayer != null) mapView.getLayerManager().getLayers().remove(pathLayer);
                    //get the new path
                    pathLayer = createPathLayer(resp);
                    //add it to the map
                    mapView.getLayerManager().getLayers().add(pathLayer);
                    mapView.getLayerManager().redrawLayers();
                } else {
                    logUser("Error:" + resp.getErrors());
                }
            }
        }.execute();
    }

    /**
     * Prints messages in the log
     * @param str the message to print
     */
    private void logUser(String str) {
        Log.i("WayComputer",str);
        Toast.makeText(context, str, Toast.LENGTH_LONG).show();
    }

    /**
     * Creates the line corresponding to the given points
     * @param response contains the points of the path
     * @return the line which will be drawn
     */
    private Polyline createPathLayer(PathWrapper response) {
        GraphicFactory gf = AndroidGraphicFactory.INSTANCE;
        Paint paint = gf.createPaint();

        //define the style of the line
        paint.setStyle(Style.STROKE);
        paint.setStrokeWidth(10);
        paint.setColor(Color.BLUE);
        Polyline pathLayer = new Polyline(paint, gf);
        List<LatLong> latLongs = pathLayer.getLatLongs();
        PointList pointList = response.getPoints();
        for (int i = 0; i < pointList.getSize(); i++)
            latLongs.add(new LatLong(pointList.getLatitude(i), pointList.getLongitude(i)));
        return pathLayer;
    }

    /**
     * Indicates if the graphe has been loaded
     * @return true if the graphe is ready false otherwise
     */
    public boolean isGraphLoaded(){
        return graphLoaded;
    }

    /**
     * Returns the current line
     * @return the ligne which is drawn on the map
     */
    public Polyline getPathLayer() {
        return pathLayer;
    }

    /**
     * Returns true if the itinerary is computable false otherwise
     * Used when the position of the user is needed
     * @param locationManager to verify if the user can be localizable
     * @param position the current position of the user on the map
     * @return true if the itinerary is computable false otherwise
     */
    public boolean isItineraryAvailable(LocationManager locationManager,LatLong position){
        boolean gpsAvailable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean ntwrkAvailable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (isGraphLoaded()) {
            //if the user is not out of the map's limits
            if(position.latitude>43.559500 && position.latitude<43.5667000 && position.longitude>1.4563000 && position.longitude<1.4767000) {
                if (gpsAvailable || ntwrkAvailable) {
                    return true;
                } else {
                    Toast.makeText(context, "Impossible de vous localiser", Toast.LENGTH_LONG).show();
                    return false;
                }
            }else{
                Toast.makeText(context, "Position hors campus", Toast.LENGTH_LONG).show();
                return false;
            }
        }else {
            Toast.makeText(context,"Itinéraire indisponible",Toast.LENGTH_LONG).show();
            return false;
        }
    }

    /**
     * Returns true if the itinerary is computable false otherwise
     * @return true if the itinerary is computable false otherwise
     */
    public boolean isItineraryAvailable(){

        if (isGraphLoaded()) return true;
        else {
            Toast.makeText(context,"Itinéraire indisponible",Toast.LENGTH_LONG).show();
            return false;
        }
    }

}
