package com.graphhopper.android;

/**
 * Class which represents a place (can be a building, a bus station...)
 */
public class Lieu {
	private float longitude;
	private float latitude;
	private String nom;
	private long id;
	/**
	 * id geographic
	 */
	private long idG;
	/**
	 * can be a building, a bus station...
	 */
	private String attribut;

	public Lieu(float pLon, float pLat, String pnom, long pid, String patt){
		this.longitude = pLon;
		this.latitude = pLat;
		this.nom = pnom;
		this.id = pid;
		this.attribut = patt;
	}

	public Lieu(String pnom, long pid){
		this.nom = pnom;
		this.id = pid;
	}

	public Lieu(long pid){
		this.id = pid;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAttribut(){
		return attribut;
	}

	public void setAttribut(String attribut){
		this.attribut = attribut;
	}

	public long getIdG() {
		return idG;
	}

	public void setIdG(long idG) {
		this.idG = idG;
	}

	@Override
	public String toString() {
		return "Lieu [longitude=" + longitude + ", latitude=" + latitude + ", nom=" + nom + ", id=" + id + ", idG="+idG+", attribut="
				+ attribut + "]";
	}



}
