package com.graphhopper.android;

import android.content.Context;
import android.support.design.widget.NavigationView;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Contains various methods
 */
public class Util {

    /**
     * Unchecks all the items of the menu
     * @param nm the view which contains the items
     */
    public void uncheckAllItem(NavigationView nm){
        int size = nm.getMenu().size();
        for (int i = 0; i < size; i++) {
            nm.getMenu().getItem(i).setChecked(false);
        }
    }

    /**
     * Moves the files from the assets folder to the cache of the application
     * or to the mentioned folder if the destination is not the cache
     * @param context the activity's context
     * @param parentFolder the path to the folder where the files will be moved
     * @param listFiles the list of the file name
     */
    public static void replaceAssetToCache(Context context, File parentFolder, String... listFiles) {
        File currentFile;
        if(!parentFolder.exists()) parentFolder.mkdir();
        String folderParentName = parentFolder.getName();
        if(folderParentName.equals("cache")) folderParentName = "";
        else folderParentName = folderParentName+"/";
        for (String fileName : listFiles) {
            currentFile = new File(parentFolder.getAbsolutePath() + "/" + fileName);
            Log.i("replaceAssetToCache", parentFolder.getAbsolutePath() + "/" + fileName);
            if (!currentFile.exists()) {
                writeFileToDir(context, currentFile, folderParentName, fileName);
            }
        }
    }

    /**
     * Writes a file to the given destination
     * @param context the activity's context
     * @param currentFile the path file once he will be moved
     * @param folderParentName the parent folder of the file
     * @param fileName the file to move
     * @throws IOException if file was not found
     */
    private static void writeFileToDir(Context context, File currentFile, String folderParentName, String fileName) {
        InputStream is;
        FileOutputStream fos;
        int size;
        byte[] buffer;

        try {
            Log.i("writeFileToCache", folderParentName + fileName);
            is = context.getAssets().open(folderParentName + fileName);
            Log.i("writeFileToCache", folderParentName + fileName);
            size = is.available();
            buffer = new byte[size];
            is.read(buffer);
            is.close();
            fos = new FileOutputStream(currentFile);
            fos.write(buffer);
            fos.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
