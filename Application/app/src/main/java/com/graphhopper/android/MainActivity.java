package com.graphhopper.android;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import org.mapsforge.core.graphics.Bitmap;
import org.mapsforge.core.model.BoundingBox;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.map.android.graphics.AndroidGraphicFactory;
import org.mapsforge.map.android.util.AndroidUtil;
import org.mapsforge.map.android.view.MapView;
import org.mapsforge.map.datastore.MapDataStore;
import org.mapsforge.map.layer.cache.TileCache;
import org.mapsforge.map.layer.overlay.Marker;
import org.mapsforge.map.layer.overlay.Polyline;
import org.mapsforge.map.layer.renderer.TileRendererLayer;
import org.mapsforge.map.reader.MapFile;
import org.mapsforge.map.rendertheme.InternalRenderTheme;

import java.io.File;
import java.util.Arrays;
import java.util.List;


public class MainActivity extends AppCompatActivity implements LocationListener {

    /**
     * Map's name in the storage
     */
    private static final String MAP_FILE = "/mapUPS.map";

    /**
     * DEBUG TAG
     */
    private static final String TAG = "MonActivite";

    //Map
    private MapView mapView;
    private TileRendererLayer tileRendererLayer;
    private WayComputer wayComputer;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private LocationManager locationManager;
    private Location location;
    private String provider;
    /**
     * Current position of the user
     */
    private LatLong position = new LatLong(0,0);
    private Marker marker;
    private Marker markerLieu;
    private Drawable drawable;
    private Bitmap bitmap;
    private Bitmap bitmapLieu;

    ///IHM
    private DrawerLayout mDrawerLayout;
    protected EditText edtActionView;
    protected ImageButton btnActionView;
    Button btnStp;
    private FloatingActionButton goButton;
    FloatingActionButton myFab;
    AutoCompleteTextView actv;
    AutoCompleteTextView actvFrom;
    ListView bats;
    DatabaseManager databaseManager;

    /**
     * no_Lieu of the selected starting point
     */
    Long idL;
    /**
     * no_Lieu of the selected ending point
     */
    Long idLFrom;
    List<Triplet<String,Long,String>> allDatas;
    /**
     * Coordinates of the starting point
     */
    LatLong latFrom ;
    /**
     * Coordinates of the ending point
     */
    LatLong latl;
    /**
     * To know if we have in itinerary mode or normal research
     */
    boolean itineraryMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        provider = locationManager.GPS_PROVIDER;
        checkLocationPermission();

        initMarker();
        initMarkerLieu();

        AndroidGraphicFactory.createInstance(getApplication());

        initMap();

        //IHM
        actv = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        actv.setThreshold(1);

        actvFrom = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextViewFrom);
        actvFrom.setThreshold(1);



        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mDrawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        switch (menuItem.getItemId()) {
                            case R.id.nav_apropos:
                                Intent intentAPropos = new Intent(MainActivity.this, A_propos.class);
                                MainActivity.this.startActivity(intentAPropos);
                                return true;
                        }

                        return true;
                    }
                });

        myFab = (FloatingActionButton) findViewById(R.id.fab);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                hideSoftKeyBoard();
                if(position.latitude>43.559500 && position.latitude<43.5667000 && position.longitude>1.4563000 && position.longitude<1.4767000) {
                    mapView.setCenter(position);
                } else if (position.latitude==0 && position.longitude==0) {
                    Toast.makeText(getApplicationContext(),"Signal GPS introuvable",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(),"Position hors campus", Toast.LENGTH_SHORT).show();
                }
            }
        });
        databaseManager = new DatabaseManager(this);
        //buttons for the itinerary
        btnStp = (Button) findViewById(R.id.buttonStop);
        goButton = (FloatingActionButton) findViewById(R.id.buttonGo);
        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = databaseManager.getReadableDatabase();
                if(!itineraryMode || (!actv.getText().toString().isEmpty() || actv.getText().toString().length() != 0 )) {
                    if (!itineraryMode) {
                        itineraryMode = true;
                        String nomBat = actv.getText().toString();
                        Log.i("IDL\\", idL + "");
                        latFrom = new LatLong(position.getLatitude(), position.getLongitude());
                        if (idL != null && itineraryMode) {
                            latl = databaseManager.getCoord(db, Integer.parseInt(idL.toString()), nomBat);
                            if (latl != null) {
                                //Toast.makeText(MainActivity.this, Double.toString(latl.latitude) + " " + Double.toString(latl.longitude), Toast.LENGTH_LONG).show();
                                Log.i("POSITION\\", Double.toString(position.latitude) + " " + Double.toString(position.longitude));
                                if (wayComputer.isItineraryAvailable(locationManager, position)) {
                                    wayComputer.calcPath(position.getLatitude(), position.getLongitude(), latl.getLatitude(), latl.getLongitude());
                                    centerPositionWithCoords(position, latl);
                                }
                            }
                        }

                    }

                    //we print a field for the destination
                    actvFrom.setVisibility(View.VISIBLE);
                    actvFrom.setText("Votre Position");
                    btnStp.setVisibility(View.VISIBLE);

                    db.close();
                }
            }
        });


        SQLiteDatabase db = databaseManager.getReadableDatabase();
        //get the datas of the first ligne
        Cursor requestRes = db.rawQuery("SELECT * FROM LIEU LIMIT 1",null);
        //if the result is zero then the database is empty
        if(requestRes.getCount() == 0){
            databaseManager.fill_BDD();
            requestRes.close();
        }
        if(!db.isOpen()) db = databaseManager.getReadableDatabase();
        allDatas = databaseManager.getAllDatas(db);
        AdapterSearch adpterSearch = new AdapterSearch(this,R.layout.layout_adapter_recherche,allDatas);
        actv.setAdapter(adpterSearch);

        actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hideSoftKeyBoard();
                Triplet<String, Long, String> lieuChoisi = (Triplet<String, Long, String>) parent.getItemAtPosition(position);
                idL = lieuChoisi.getSnd();
                Log.i("IDLACTV\\", idL + "");
                SQLiteDatabase db = databaseManager.getReadableDatabase();
                latl = databaseManager.getCoord(db, Integer.parseInt(idL.toString()), actv.getText().toString());

                //remove the itinerary
                Polyline pathLayer = wayComputer.getPathLayer();
                if (pathLayer != null) mapView.getLayerManager().getLayers().remove(pathLayer);

                if (itineraryMode) {
                    if (latFrom == null) {
                        if(MainActivity.this.position.getLatitude() != 0){
                            latFrom = new LatLong(MainActivity.this.position.getLatitude(),MainActivity.this.position.getLongitude());
                        }
                        else Toast.makeText(MainActivity.this, "Aucune destination de départ indiquée", Toast.LENGTH_LONG).show();
                    }
                    if (actvFrom.getText().toString().equals(R.string.position)) {
                        drawPath(actv.getText().toString(),1,true);
                    }else {
                        drawPath(actv.getText().toString(),1,false);
                    }
                } else {
                    //center to the place
                    centerPosition(idL, actv);
                }

                db.close();
            }


        });
        initActvFrom();

    }

    /**
     * Initializes the listener of the second search bar
     */
    public void initActvFrom(){
        AdapterSearch adpterSearch = new AdapterSearch(MainActivity.this, R.layout.layout_adapter_recherche, allDatas);
        actvFrom.setAdapter(adpterSearch);
        actvFrom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hideSoftKeyBoard();
                String nomBat = actvFrom.getText().toString();
                Triplet<String, Long, String> lieuChoisi = (Triplet<String, Long, String>) parent.getItemAtPosition(position);
                idLFrom = lieuChoisi.getSnd();
                Log.i("IDLACTV\\", idL + "");
                if (latl != null) {
                    //remove the itinerary
                    Polyline pathLayer = wayComputer.getPathLayer();
                    if (pathLayer != null) mapView.getLayerManager().getLayers().remove(pathLayer);
                    //center to the place
                    centerPosition(idLFrom, actvFrom);
                }
                drawPath(nomBat, 2, false);
            }
        });
    }

    /**
     * Draws the itinerary if it is possible
     * @param nomBat buidings's name
     * @param use the coordinates to update (begin or end)
     * @param verifLocation true if we want to verify the current position
     */
    public void drawPath(String nomBat, int use,boolean verifLocation){
            SQLiteDatabase db = databaseManager.getReadableDatabase();
            if(use == 1){
                latl = databaseManager.getCoord(db, Integer.parseInt(idL.toString()), nomBat);
                if(latFrom == null || latFrom.getLatitude() == 0.0) return;
            }else {
                latFrom = databaseManager.getCoord(db, Integer.parseInt(idLFrom.toString()), nomBat);
                if(latl == null) return;
            }
            if(idL != null) {
                if(!verifLocation) {
                    if (wayComputer.isItineraryAvailable()) {
                        wayComputer.calcPath(latFrom.getLatitude(), latFrom.getLongitude(), latl.getLatitude(), latl.getLongitude());
                        centerPositionWithCoords(latFrom,latl);
                    }
                }else{
                    if (wayComputer.isItineraryAvailable(locationManager,position)) {
                        wayComputer.calcPath(latFrom.getLatitude(), latFrom.getLongitude(), latl.getLatitude(), latl.getLongitude());
                        centerPositionWithCoords(latFrom,latl);
                    }
                }
                db.close();
            }else{
                Toast.makeText(MainActivity.this, "Aucune destination indiquée", Toast.LENGTH_LONG).show();
            }
    }

    /**
     * Stops the itinerary
     * @param view itinerary's view
     */
    public void stopperItineraire(View view){
        actvFrom.setVisibility(View.GONE);
        btnStp.setVisibility(View.GONE);
        itineraryMode = false;
        //remove the itinerary
        Polyline pathLayer = wayComputer.getPathLayer();
        if(pathLayer != null) mapView.getLayerManager().getLayers().remove(pathLayer);
    }

    /**
     * Finds the selected button
     * @param view the selected button
     */
    public void choixElement(View view) {

        //get the button clicked
        Button buttonCateg = (Button)view;
        SQLiteDatabase db = databaseManager.getReadableDatabase();
        //get the datas according to a category
        List<Couple<Long,String>> allDatas = databaseManager.getAllDatasByType(db,buttonCateg.getText().toString());
        Log.i("ALLDATASCATEG",Arrays.toString(allDatas.toArray()));

        //create a pop-up to print the datas in a ListView
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.layout_dialog_recherche);
        dialog.setTitle("Recherche du "+buttonCateg.getText().toString());
        bats= (ListView) dialog.findViewById(R.id.list_dialog);

        //create an adapter to print the datas
        AdapaterCateg adapterSimple = new AdapaterCateg(MainActivity.this,android.R.layout.simple_list_item_1, allDatas);
        bats.setAdapter(adapterSimple);
        dialog.show(); //print the pop-up

        //when a click on an item is detected
        bats.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //get the item
                Couple<Long,String> lieuChoisi = (Couple<Long,String>) parent.getItemAtPosition(position);
                actv.setText(lieuChoisi.getLst());
                actv.dismissDropDown(); // avoid that the Autocomplete view did a research
                idL = lieuChoisi.getFst(); // get the id of the place to use it with the itinerary button
                centerPosition(idL,actv);
                dialog.dismiss(); // close the dialog box

                //if we want to compute an itinerary between two buildings throught the dialog box
                btnStp.setVisibility(View.GONE);
                itineraryMode = false;
                //remove the itinerary
                Polyline pathLayer = wayComputer.getPathLayer();
                if(pathLayer != null) mapView.getLayerManager().getLayers().remove(pathLayer);


            }
        });
        db.close();

    }

    /**
     * Centers to the selected area and draw a marker
     * @param id the id of the place
     * @param autoCompleteTextView contains the selected item
     */
    public void centerPosition (Long id,AutoCompleteTextView autoCompleteTextView) {
        String nomBat =  autoCompleteTextView.getText().toString();
        SQLiteDatabase db = databaseManager.getReadableDatabase();
        LatLong latl = databaseManager.getCoord(db, Integer.parseInt(id.toString()),nomBat);
        putMarker(latl);
        mapView.setZoomLevel((byte)19);
        mapView.setCenter(latl);
        db.close();
    }

    /**
     * Centers to the given coordinates and draw a marker
     * @param centerCoords the coordinates of the given place
     * @param markerPlace the coordinates where the marker will be drawn
     */
    public void centerPositionWithCoords (LatLong centerCoords,LatLong markerPlace) {
        putMarker(markerPlace);
        mapView.setZoomLevel((byte)19);
        mapView.setCenter(centerCoords);
    }

    /**
     * Initializes the map
     */
    private void initMap() {
        mapView = findViewById(R.id.mapView);

        Log.d(TAG, getCacheDir().toString());
        try {

            mapView.setClickable(true);
            mapView.getMapScaleBar().setVisible(true);
            mapView.setBuiltInZoomControls(false);

            TileCache tileCache = AndroidUtil.createTileCache(this, "mapcache",
                    mapView.getModel().displayModel.getTileSize(), 1f,
                    mapView.getModel().frameBufferModel.getOverdrawFactor());
            File f = new File(getCacheDir() + MAP_FILE);

            Util.replaceAssetToCache(this,getCacheDir(),"mapUPS.map");
            if (f.exists()) {
                Log.d(TAG, "fichier map trouvé");
            } else {
                Log.d(TAG, "fichier map pas trouvé");
            }
            final MapDataStore mapDataStore = new MapFile(f);
            tileRendererLayer = new TileRendererLayer(tileCache, mapDataStore,
                    mapView.getModel().mapViewPosition, AndroidGraphicFactory.INSTANCE);
            tileRendererLayer.setXmlRenderTheme(InternalRenderTheme.DEFAULT);
            wayComputer = new WayComputer(this, MAP_FILE.substring(0, MAP_FILE.indexOf(".")), mapView);


            mapView.getLayerManager().getLayers().add(tileRendererLayer);
            mapView.setZoomLevelMin((byte) 17);
            mapView.setZoomLevelMax((byte) 19);

            mapView.setZoomLevel((byte) 17);
            mapView.getModel().mapViewPosition.setMapLimit(new BoundingBox(43.559500, 1.4563000, 43.5667000, 1.4767000));

            mapView.setCenter(new LatLong(43.5633293, 1.4659036));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            locationManager.requestLocationUpdates(provider, 400, 1, this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            locationManager.removeUpdates(this);
        }
    }


    @Override
    protected void onDestroy() {
        databaseManager.close();
        mapView.destroyAll();
        AndroidGraphicFactory.clearResourceMemoryCache();
        super.onDestroy();
    }

    /**
     * Activated when the user opens the menu bar
     * @param item the lateral menu
     * @return true if a button was chosen otherwise returns the result of the superclass methods
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Indicates if the application have access to the location
     * @return true if the application can access to the location false otherwise
     */
    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Permission")
                        .setMessage("Pour avoir accès à la géolocalisation, merci d'autoriser la permission.")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    /**
     * asks the permission to use the location of the user
     * @param requestCode the required permissions
     * @param grantResults the response of the user
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        locationManager.requestLocationUpdates(provider, 400, 1, this);
                        Criteria criteria = new Criteria();
                        criteria.setAccuracy(Criteria.ACCURACY_FINE);
                        criteria.setCostAllowed(false);
                        location = locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
                        if(location != null){
                            position = new LatLong(location.getLatitude(),location.getLongitude());
                        } else {
                            Log.i("bug","bug");
                        }
                    }

                } else {
                    Toast.makeText(getApplicationContext(),"Les fonctionnalités utilisant la géolocalisation ont été désactivés",Toast.LENGTH_LONG).show();
                    myFab.setVisibility(View.INVISIBLE);
                }
                return;
            }

        }
    }

    /**
     * Called when the location has changed.
     * <p>
     * <p> There are no restrictions on the use of the supplied Location object.
     *
     * @param location The new location, as a Location object.
     */
    @Override
    public void onLocationChanged(Location location) {

        Double lat = location.getLatitude();
        Double lng = location.getLongitude();
        position = new LatLong(lat,lng);

        putLocationMarker(position);
        Log.i("Location info: Lat", lat.toString());
        Log.i("Location info: Lng", lng.toString());

    }

    /**
     * Called when the provider status changes. This method is called when
     * a provider is unable to fetch a location or if the provider has recently
     * become available after a period of unavailability.
     *
     * @param provider the name of the location provider associated with this
     *                 update.
     * @param status   {@link LocationProvider#OUT_OF_SERVICE} if the
     *                 provider is out of service, and this is not expected to change in the
     *                 near future; {@link LocationProvider#TEMPORARILY_UNAVAILABLE} if
     *                 the provider is temporarily unavailable but is expected to be available
     *                 shortly; and {@link LocationProvider#AVAILABLE} if the
     *                 provider is currently available.
     * @param extras   an optional Bundle which will contain provider specific
     *                 status variables.
     *                 <p>
     *                 <p> A number of common key/value pairs for the extras Bundle are listed
     *                 below. Providers that use any of the keys on this list must
     *                 provide the corresponding value as described below.
     *                 <p>
     *                 <ul>
     *                 <li> satellites - the number of satellites used to derive the fix
     */
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    /**
     * Called when the provider is enabled by the user.
     *
     * @param provider the name of the location provider associated with this
     *                 update.
     */
    @Override
    public void onProviderEnabled(String provider) {

    }

    /**
     * Called when the provider is disabled by the user. If requestLocationUpdates
     * is called on an already disabled provider, this method is called
     * immediately.
     *
     * @param provider the name of the location provider associated with this
     *                 update.
     */
    @Override
    public void onProviderDisabled(String provider) {

    }

    /**
     * Initializes the marker which will be used by the map
     */
    public void initMarker(){
        drawable = getResources().getDrawable(R.drawable.ic_navigation_black_24dp);
        bitmap = AndroidGraphicFactory.convertToBitmap(drawable);
        marker = new Marker(new LatLong(0,0),bitmap,0,0);
    }

    /**
     * Draws a marker at the given position
     * @param latlong the coordinates where the marker will be put
     */
    public void putLocationMarker(LatLong latlong){

        if(mapView.getLayerManager().getLayers().contains(marker)){
            mapView.getLayerManager().getLayers().remove(marker);
        }

        marker = new Marker(latlong,bitmap,0,0);
        mapView.getLayerManager().getLayers().add(marker);

    }

    /**
     * Initializes the marker which will be used by the map
     */
    public void initMarkerLieu(){
        drawable = getResources().getDrawable(R.drawable.ic_place_red_40dp);
        bitmapLieu = AndroidGraphicFactory.convertToBitmap(drawable);
        markerLieu = new Marker(new LatLong(0,0),bitmapLieu,0,0);
    }

    /**
     * Draws a marker at the given position
     * @param latlong the coordinates where the marker will be put
     */
    public void putMarker(LatLong latlong){

        if(mapView.getLayerManager().getLayers().contains(markerLieu)){
            mapView.getLayerManager().getLayers().remove(markerLieu);
        }

        markerLieu = new Marker(latlong,bitmapLieu,0,0);
        mapView.getLayerManager().getLayers().add(markerLieu);

    }

    /**
     * Hides the keyboard from the screen
     */
    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if(imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }


}

