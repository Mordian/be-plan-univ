package com.graphhopper.android;

/**
 * Generic class with three parameters
 */
public class Triplet<T,U,V> {

    private T fst;
    private U snd;
    private V trd;

    public Triplet(T pfst,U psnd, V ptrd){
        fst = pfst;
        snd = psnd;
        trd = ptrd;
    }

    public T getFst() {
        return fst;
    }

    public U getSnd() {
        return snd;
    }

    public V getTrd() {
        return trd;
    }
}
