package com.graphhopper.android;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

class AdapaterCateg extends ArrayAdapter<Couple<Long,String>> {
    private List<Couple<Long,String>> lLieux;
    private final  Context mContext;
    private final int mLayoutResourceId;


    public AdapaterCateg(@NonNull Context context, int resource, List<Couple<Long,String>> datas) {
        super(context, resource, datas);
        this.lLieux = new ArrayList<>(datas);
        this.mContext = context;
        mLayoutResourceId = resource;
    }

    @Override
    public int getCount() {
        return lLieux.size();
    }

    @Override
    public Couple<Long,String> getItem(int position) {
        return lLieux.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(mLayoutResourceId, parent, false);
        }

        Couple<Long,String> lieu = getItem(position);
        TextView nom = (TextView)convertView.findViewById(android.R.id.text1); // TextView du layout simple_list_item_1
        nom.setText(lieu.getLst());

        return convertView;
    }
}
