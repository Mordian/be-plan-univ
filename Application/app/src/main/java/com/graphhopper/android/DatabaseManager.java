package com.graphhopper.android;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.mapsforge.core.model.LatLong;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Class which manages the database
 */
public class DatabaseManager extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "places.db";
    /**
     * The request to create the table
     */
    public static final String LIEU_TABLE_CREATE =
            "CREATE TABLE Lieu ("+
                    "no_Lieu INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "noG_Lieu INTEGER,"+
                    "nom_Lieu TEXT, " +
                    "categ_Lieu TEXT,"+
                    "lat_Lieu TEXT, "+
                    "long_Lieu TEXT);";
    /**
     * The request to remove the table
     */
    public static final String LIEU_TABLE_DROP = "DROP TABLE IF EXISTS Lieu;";
    private Context mContext;

    /**
     * Creates the database
     * @param context the activity's context
     */
    public DatabaseManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    /**
     * Calls the request to create the table if the database does not exist
     * @throws SQLException if an error occured during the creation
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(LIEU_TABLE_CREATE);
        }catch(SQLException e){
            e.printStackTrace();
        }
        Log.i("Create",LIEU_TABLE_CREATE);
    }

    /**
     * Updates the database if the new version number is higher than the old version number
     * @param db the database in read mode
     * @param oldVersion old version number of the database
     * @param newVersion new version number of the database
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(LIEU_TABLE_DROP);
        onCreate(db);
    }

    /**
     * Adds the places to the database
     * @throws IOException
     */
    public void fill_BDD(){
        SQLiteDatabase db = this.getWritableDatabase();
        AssetManager assetManager = mContext.getAssets();

        try {
            InputStream is = assetManager.open("requeteinserts.sql");
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            //get the file lines and execute the requests
            while ((line = br.readLine()) != null) {
                db.execSQL(line);
            }
            br.close();
        } catch (IOException e) {
            Log.i( "EXEC","IOException thrown while attempting to "
                    + "create database.");
        }
        db.close();
    }

    /**
     * Returns the place corresponding to the given id
     * or null if he doesn't exist
     * @param db the database in read mode
     * @param id the place's identifier
     * @return the place corresponding to the given id
     */
    public Lieu getAllFromId(SQLiteDatabase db,long id){
        String sid = String.valueOf(id);
        Cursor c = db.rawQuery("SELECT noG_Lieu, nom_Lieu, categ_Lieu, lat_Lieu, long_Lieu FROM Lieu WHERE no_Lieu = '"+sid+"'", null);
        Lieu resultLieu = new Lieu(id);
        if(c.moveToFirst() && c.getCount() > 0){
            resultLieu.setIdG(c.getLong(c.getColumnIndex("noG_Lieu")));
            resultLieu.setNom(c.getString(c.getColumnIndex("nom_Lieu")));
            resultLieu.setAttribut(c.getString(c.getColumnIndex("categ_Lieu")));
            resultLieu.setLatitude(Float.parseFloat(c.getString(c.getColumnIndex("lat_Lieu"))));
            resultLieu.setLongitude(Float.parseFloat(c.getString(c.getColumnIndex("long_Lieu"))));
            c.close();
            return resultLieu;
        } else {
            c.close();
            return null;
        }
    }

    /**
     * Returns the list of identifiers, names and categories of the places of the database
     * or null if database is empty
     * @param db the database in read mode
     * @return the list of identifiers, names and categories of the places
     */
    public List<Triplet<String,Long,String>> getAllDatas(SQLiteDatabase db){
        Cursor c = db.rawQuery("SELECT no_Lieu, nom_Lieu, categ_Lieu FROM Lieu", null);
        List<Triplet<String,Long,String>> datas = new ArrayList<>();
        Long id;
        String name;
        String attr;
        if(c.moveToFirst() && c.getCount() > 0){
            do {
                id = c.getLong(c.getColumnIndex("no_Lieu"));
                name = c.getString(c.getColumnIndex("nom_Lieu"));
                attr = c.getString(c.getColumnIndex("categ_Lieu"));
                datas.add(new Triplet<String, Long, String>(name,id,attr));
                Log.i("ALLDATAS", id+" "+name+" "+attr);
            }while(c.moveToNext());
            c.close();
            return datas;
        } else {
            c.close();
            return null;
        }
    }

    /**
     * Gets all the datas for a given category
     * or null if there is no place which correspond
     * @param db the database in read mode
     * @param catButton the button which has been clicked
     * @return the list of the identifiers and names of the places
     */
    public List<Couple<Long,String>> getAllDatasByType (SQLiteDatabase db, String catButton) {
        String requete;
        if (catButton.equalsIgnoreCase("Batiment")){
            requete = "SELECT no_Lieu, nom_Lieu FROM Lieu WHERE categ_Lieu = 'WIP'";
        }else if (catButton.equalsIgnoreCase("Amphi")){
            requete = "SELECT no_Lieu, nom_Lieu FROM Lieu WHERE categ_Lieu = 'amphi'";
        }else{
            requete = "SELECT no_Lieu, nom_Lieu FROM Lieu WHERE categ_Lieu != 'amphi' and categ_Lieu != 'WIP'";
        }
        Log.i("REQUETE",requete);
        Cursor c = db.rawQuery(requete, null);
        List<Couple<Long,String>> datas = new ArrayList<>();
        Long id;
        String name;
        if(c.moveToFirst() && c.getCount() > 0){
            do {
                id = c.getLong(c.getColumnIndex("no_Lieu"));
                name = c.getString(c.getColumnIndex("nom_Lieu"));
                datas.add(new Couple<Long,String>(id,name));
                Log.i("ALLDATAS", id+" "+name);
            }while(c.moveToNext());
            c.close();
            return datas;
        } else {
            c.close();
            return null;
        }
    }

    /**
     * Returns the coordinates corresponding to the given identifier and name
     * or null if there is no place which correspond
     * @param db the database in read mode
     * @param id the identifier of the place
     * @param name the name of the place
     * @return the coordinates of the place or null if there is no place which correspond
     */
    public LatLong getCoord(SQLiteDatabase db, int id, String name){
        String sid = String.valueOf(id);
        String lowName = name;
        Cursor c = db.rawQuery("SELECT lat_Lieu, long_Lieu FROM Lieu WHERE no_Lieu = '"+sid+"' AND nom_Lieu = '"+lowName+"'", null);
        if(c.moveToFirst() && c.getCount() > 0){
            Float lat = Float.parseFloat(c.getString(c.getColumnIndex("lat_Lieu")));
            Float longi = Float.parseFloat(c.getString(c.getColumnIndex("long_Lieu")));
            LatLong coords = new LatLong(lat,longi);
            c.close();
            return coords;
        } else {
            c.close();
            return null;
        }
    }
}
