package com.graphhopper.android;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class which prints the results of the research
 */
public class AdapterSearch extends ArrayAdapter<Triplet<String,Long,String>> {

    private List<Triplet<String,Long,String>> lLieux;
    private final  Context mContext;
    private List<Triplet<String,Long,String>> lLieuxAll;
    private final int mLayoutResourceId;

    public AdapterSearch(Context context, int resource, List<Triplet<String,Long,String>> lieux){
        super(context, resource, lieux);
        this.lLieux = new ArrayList<>(lieux);
        this.lLieuxAll = new ArrayList<>(lieux);
        this.mContext = context;
        mLayoutResourceId = resource;
    }

    /**
     * returns the number of places
     */
    @Override
    public int getCount() {
        return lLieux.size();
    }

    /**
     * Returns the place at the given position
     * @param position index of the place
     * @throws IndexOutOfBoundsException if the index does not correspond to a value
     */
    @Override
    public Triplet<String,Long,String> getItem(int position) {
        return lLieux.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(mLayoutResourceId, parent, false);
        }

        Triplet<String,Long,String> lieu = getItem(position);
        TextView nom = (TextView)convertView.findViewById(R.id.textViewNomBat);
        nom.setText(lieu.getFst());

        TextView type = (TextView)convertView.findViewById(R.id.textViewType);
        type.setText(lieu.getTrd());

        TextView id = (TextView)convertView.findViewById(R.id.textViewIdTag);
        id.setText(lieu.getSnd().toString());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            public String convertResultToString(Object resultValue) {
                return ((Triplet<String,Long,String>) resultValue).getFst();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                List<Triplet<String,Long,String>> lieuSuggestion = new ArrayList<>();
                if (constraint != null) {
                    for (Triplet<String,Long,String> lieu : lLieuxAll) {
                        if (lieu.getFst().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                            lieuSuggestion.add(lieu);
                        }
                    }
                    filterResults.values = lieuSuggestion;
                    filterResults.count = lieuSuggestion.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                lLieux.clear();
                if (results != null && results.count > 0) {
                    // avoids unchecked cast warning when using mDepartments.addAll((ArrayList<Department>) results.values);
                    for (Object object : (List<?>) results.values) {
                        if (object instanceof Triplet<?,?,?>) {
                            lLieux.add((Triplet<String,Long,String>) object);
                        }
                    }
                    notifyDataSetChanged();
                } else if (constraint == null) {
                    // no filter, add entire original list back in
                    lLieux.addAll(lLieuxAll);
                    notifyDataSetInvalidated();
                }
            }
        };
    }
}
