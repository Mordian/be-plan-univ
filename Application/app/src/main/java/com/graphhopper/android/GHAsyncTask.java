package com.graphhopper.android;

import android.os.AsyncTask;

/**
 * To execute asynchronous task
 * @param <A> parameter's types given to the task
 * @param <B> data's type given during the progression of the treatment
 * @param <C> result's types of the task
 */
public abstract class GHAsyncTask<A, B, C> extends AsyncTask<A, B, C> {
    private Throwable error;

    protected abstract C saveDoInBackground(A... params) throws Exception;

    /**
     * Execute a treatment in background
     */
    protected C doInBackground(A... params) {
        try {
            return saveDoInBackground(params);
        } catch (Throwable t) {
            error = t;
            return null;
        }
    }

    /**
     * Returns true if an error occured
     * @return true if an error occured false otherwise
     */
    public boolean hasError() {
        return error != null;
    }

}
