package com.graphhopper.android;

/**
 * Generic class with two parameters
 */
public class Couple<T,U> {
    private T fst;
    private U lst;

    public Couple(T fst,U lst){
        this.fst = fst;
        this.lst = lst;
    }

    public T getFst() {
        return fst;
    }

    public void setFst(T fst) {
        this.fst = fst;
    }

    public U getLst() {
        return lst;
    }

    public void setLst(U lst) {
        this.lst = lst;
    }

    public String toString(){
        return " ["+fst+","+lst+"]";
    }
}
