#!/bin/sh
#On va dans le repertoire ou on fait les manipulation
#cd /home/Java -> changer cette ligne selon l'arborescence de la machine
#On recupere la date du jour
currentDate=$(date +%Y-%m-%d)
#echo "$d"
#On cree un copie de sauvegarde
cp map.osm mapOld.osm
#On import la nouvelle map
# WIP java -jar ./importMap
#On compare la mapNew et la mapOld
diff > diffResult.txt
#On lit le fichier de différences
java -jar ./output.jar
result=$?
if [ "$result" == "2c2" ]; then
	#On supprime les fichiers neufs car ils n'y pas de modifications à faire
	rm mapNew.osm
  rm mapOld.osm
  rm $filename
else
	#On archive les vieux fichiers
	cp map.osm ArchiveOSM/osm-$currenDate.osm
  cp mapForge.map ArchiveMAP/map-$currenDate.map
  cp $filename changeLog/$filename
  rm map.osm
  rm mapOld.osm
  cp mapNew.osm map.osm
  rm mapNew.osm
  rm $filename
	#Work In Progress
	#Il faut appeler Graphhopper sur le nouveau osm pour generer le nouveau graph
  #Work in Progress
fi
echo $result
