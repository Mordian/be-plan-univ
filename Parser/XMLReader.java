package database;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLReader {
	Document doc;
	XPath xpath;

	public XMLReader() throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		XPathFactory xpathFactory = XPathFactory.newInstance();
		XPath xpath2 = xpathFactory.newXPath();
		this.doc = builder.parse("mapUPS.osm");
		this.xpath = xpath2;
	}

	private void writeStringIntoFile(String str){
		try {
			FileWriter file = new FileWriter("requeteinserts.sql");
			file.write(str);
			file.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}

	public void insertValues(List<Lieu> lieux){
		String request = "";
		StringBuilder name;
		int id;
		System.out.println("ecriture en cours");
		for (Lieu l : lieux) {
			String[] tabNom = l.getNom().split(",");
			for (String s : tabNom) {
				id = s.indexOf("'");
				if(id != -1) {
					name = new StringBuilder(s);
					while(id != -1) {
						System.out.println("premiere "+id);
						name.insert(id+1, "'");
						id = s.indexOf("'", id+2);
						System.out.println("suivante "+id);
					}
					s = name.toString();
				}
				request += "Insert into Lieu (noG_Lieu,nom_Lieu,categ_Lieu,lat_Lieu,long_Lieu) values(" + l.getIdG() + ",'" +  s + "','" + l.getAttribut() + "','" + l.getLatitude() + "','" + l.getLongitude() + "');\n";
				System.out.println("Insert into Lieu (noG_Lieu,nom_Lieu,categ_Lieu,lat_Lieu,long_Lieu) values(" + l.getIdG() + ",'" +  s + "','" + l.getAttribut() + "','" + l.getLatitude() + "','" + l.getLongitude() + "');");
			}
		}
		writeStringIntoFile(request);
		System.out.println("finis");
	}

	/**
	 * Fonctions grosses categories
	 * @return Une liste de Lieu correspondant au tag building
	 */
	public List<Lieu> getBuilding() {
		List<Lieu> list = new ArrayList<>();
		try {
			//Amphi
			XPathExpression expr = xpath.compile("/osm/way/tag[@k='building']/parent::way/@id");
			NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			System.out.println("Avant for 1");
			for(int i = 0; i< nodes.getLength();i++){


				long id = Long.parseLong(nodes.item(i).getNodeValue());
				Lieu l = new Lieu(id);
				String spid = String.valueOf(id);
				l.setNom(xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='building']/parent::way/tag[@k='name']/@v", doc));

				l.setIdG(Long.parseLong(this.xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='building']/parent::way/*[1]/@ref", doc)));
				l.setAttribut("WIP");
				this.setLatAndLon(l, doc, xpath);
				if(l.getNom()!="") {
					list.add(l);
				}
			}
			System.out.println("fin fct");
	} catch (XPathExpressionException e) {
		e.printStackTrace();
	}
	return list;
	}
	//Fonction pour avoir les associations
	public List<Lieu> getAssoc() {
		List<Lieu> list = new ArrayList<>();
		list.addAll(getAssociation());
		return list;
	}

	/**
	 * Fonction pour avoir les services
	 * @return Une liste de Lieu correspondants aux tag Orientation professionnelle - Restaurant - Bibliotheque
	 */
	public List<Lieu> getServices() {
		List<Lieu> list = new ArrayList<>();
		list.addAll(getOrientationProfessionnelle());
		list.addAll(getRestaurant());
		list.addAll(getPosteDeSecours());
		list.addAll(getBibliotheque());
		return list;
	}

	/**
	 * Fonction pour avoir les transports
	 * @return Une liste de Lieu correspondants aux tagx Metro - Bus - BusBis - Vlib
	 */
	public List<Lieu> getTransport() {
		List<Lieu> list = new ArrayList<>();
		list.addAll(getMetro());
		list.addAll(getBus());
		list.addAll(getVlib());
		return list;
	}

	/**
	 * Fonction pour avoir les batiments administatifs
	 * @return Une liste de Lieu correspondants aux tags Secretariat - SecretariatBis
	 */
	public List<Lieu> getAdministration() {
		List<Lieu> list = new ArrayList<>();
		list.addAll(this.getSecretariat());
		list.addAll(this.getSecretariatBis());
		return list;
	}

	/**
	 * Fonction pour avoir tous les batiments
	 * @return Une liste de tous les Lieu utilisés par l'application
	 */
	public List<Lieu> getAllBat(){
		List<Lieu> list = new ArrayList<>();
		list.addAll(getBuilding());
		list.addAll(getAmphis());
		list.addAll(getAssoc());
		list.addAll(getServices());
		list.addAll(getTransport());
		list.addAll(getAdministration());
		list.addAll(getAutres());
		list.addAll(getBatRecherche());
		return list;
	}


	//fonctions pour avoir une seule categorie
	//Services

	/**
	 * Fonction pour avoir les batiments relatifs à l'orientation
	 * @return Une liste de Lieu correspondants au tag Orientation Professionnelle
	 */
	public List<Lieu> getOrientationProfessionnelle() {
		List<Lieu> list = new ArrayList<>();
		try {
			//SCD
			//On recupere l'id de Way
			XPathExpression expr = this.xpath.compile("/osm/way/tag[@k='orientation professionelle']/parent::way/@id");
			NodeList nodes = (NodeList) expr.evaluate(this.doc, XPathConstants.NODESET);
			for(int i = 0; i< nodes.getLength();i++){
				long id = Long.parseLong(nodes.item(i).getNodeValue());
				Lieu l = new Lieu(id);
				l.setAttribut("orientation professionnelle");
				list.add(l);
			}
			for(int i = 0; i< nodes.getLength();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				long idG = Long.parseLong(this.xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='orientation professionelle']/parent::way/*[1]/@ref", doc));
				list.get(i).setIdG(idG);
			}
			for(int i = 0; i< list.size();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				String nom = this.xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='orientation professionelle']/@v", doc);
				list.get(i).setNom(nom);
			}
			for(int i = 0; i< list.size();i++){
				this.setLatAndLon(list.get(i),doc,xpath);
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Fonction pour avoir les restaurants
	 * @return Une liste de Lieu correspondants au tag Restaurant
	 */
	public List<Lieu> getRestaurant() {
		List<Lieu> list = new ArrayList<>();
		try {
			//restaurant universitaire
			XPathExpression expr = this.xpath.compile("/osm/way/tag[@k='restaurant']/parent::way/@id");
			NodeList nodes = (NodeList) expr.evaluate(this.doc, XPathConstants.NODESET);
			for(int i = 0; i< nodes.getLength();i++){
				long id = Long.parseLong(nodes.item(i).getNodeValue());
				Lieu l = new Lieu(id);
				l.setAttribut("restaurant");
				list.add(l);
			}
			for(int i = 0; i< nodes.getLength();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				long idG = Long.parseLong(this.xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='restaurant']/parent::way/*[1]/@ref", doc));
				list.get(i).setIdG(idG);

			}
			for(int i = 0; i< list.size();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				String nom = this.xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='restaurant']/@v", doc);
				list.get(i).setNom(nom);
			}
			for(int i = 0; i< list.size();i++){
				this.setLatAndLon(list.get(i),doc,xpath);
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Fonction pour avoir le poste de secours
	 * @return Une liste de Lieu correspondants au tag posteDeSecours
	 */
	public List<Lieu> getPosteDeSecours() {
		List<Lieu> list = new ArrayList<>();
		try {
			//poste de secours
			XPathExpression expr = xpath.compile("/osm/way/tag[@k='posteDeSecours']/parent::way/@id");
			NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			for(int i = 0; i< nodes.getLength();i++){
				long id = Long.parseLong(nodes.item(i).getNodeValue());
				Lieu l = new Lieu(id);
				l.setAttribut("poste de secours");
				list.add(l);
			}
			for(int i = 0; i< nodes.getLength();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				long idG = Long.parseLong(this.xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='posteDeSecours']/parent::way/*[1]/@ref", doc));
				list.get(i).setIdG(idG);
			}
			for(int i = 0; i< list.size();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				String nom = xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='posteDeSecours']/@v", doc);
				list.get(i).setNom(nom);
			}
			for(int i = 0; i< list.size();i++){
				this.setLatAndLon(list.get(i),doc,xpath);
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Fonction pour avoir les bibliotheques
	 * @return Une liste de Lieu correspondants au tag bibliotheque
	 */
	public List<Lieu> getBibliotheque() {
		List<Lieu> list = new ArrayList<>();
		try {
			//bibliotheque
			XPathExpression expr = xpath.compile("/osm/way/tag[@k='bibliotheque']/parent::way/@id");
			NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			for(int i = 0; i< nodes.getLength();i++){
				long id = Long.parseLong(nodes.item(i).getNodeValue());
				Lieu l = new Lieu(id);
				l.setAttribut("bibliotheque");
				list.add(l);
			}
			for(int i = 0; i< nodes.getLength();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				long idG = Long.parseLong(this.xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='bibliotheque']/parent::way/*[1]/@ref", doc));
				list.get(i).setIdG(idG);
			}
			for(int i = 0; i< list.size();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				String nom = xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='bibliotheque']/@v", doc);
				list.get(i).setNom(nom);
			}
			for(int i = 0; i< list.size();i++){
				this.setLatAndLon(list.get(i),doc,xpath);
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return list;
	}

	//TRANSPORTS
	/**
	 * Fonction pour avoir les stations de metro
	 * @return Une liste de Lieu correspondants au tag metro
	 */
	public List<Lieu> getMetro() {
		List<Lieu> list = new ArrayList<>();
		try {
			//metro
			XPathExpression expr = xpath.compile("/osm/node/tag[@k='metro']/parent::node/@id");
			NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			for(int i = 0; i< nodes.getLength();i++){
				long id = Long.parseLong(nodes.item(i).getNodeValue());
				Lieu l = new Lieu(id);
				l.setIdG(id);
				l.setAttribut("metro");
				list.add(l);
			}
			for(int i = 0; i< list.size();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				String nom = xpath.evaluate("/osm/node[@id="+spid+"]/tag[@k='metro']/@v", doc);
				list.get(i).setNom(nom);
			}
			for(int i = 0; i< list.size();i++){
				this.setLatAndLon(list.get(i),doc,xpath);
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Fonction pour avoir les stations de bus
	 * @return Une liste de Lieu correspondants au tag busBis
	 */
	public List<Lieu> getBus() {
		List<Lieu> list = new ArrayList<>();
		try {
			//metro
			XPathExpression expr = xpath.compile("/osm/node/tag[@k='busBis']/parent::node/@id");
			NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			for(int i = 0; i< nodes.getLength();i++){
				long id = Long.parseLong(nodes.item(i).getNodeValue());
				Lieu l = new Lieu(id);
				l.setIdG(id);
				l.setAttribut("Bus");
				list.add(l);
			}
			for(int i = 0; i< list.size();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				String nom = xpath.evaluate("/osm/node[@id="+spid+"]/tag[@k='busBis']/@v", doc);
				list.get(i).setNom(nom);
			}
			for(int i = 0; i< list.size();i++){
				this.setLatAndLon(list.get(i),doc,xpath);
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Fonction pour avoir les stations de Velo libres
	 * @return Une liste de Lieu correspondants au tag Vlib
	 */
	public List<Lieu> getVlib() {
        List<Lieu> list = new ArrayList<>();
        try {
            //V-lib
            XPathExpression expr = xpath.compile("/osm/node/tag[@k='Vlib']/parent::node/@id");
            NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
            for(int i = 0; i< nodes.getLength();i++){
                long id = Long.parseLong(nodes.item(i).getNodeValue());
                Lieu l = new Lieu(id);
                l.setIdG(id);
                l.setAttribut("Vlib");
                list.add(l);
            }
            for(int i = 0; i< list.size();i++){
                long pid = list.get(i).getId();
                String spid = String.valueOf(pid);

                String nom = xpath.evaluate("/osm/node[@id="+spid+"]/tag[@k='Vlib']/parent::node/tag[@k='name']/@v", doc);
                list.get(i).setNom(nom);
            }
            for(int i = 0; i< list.size();i++){
                this.setLatAndLon(list.get(i),doc,xpath);
            }
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        return list;
    }

	//VIE ETUDIANTE
	/**
	 * Fonction pour avoir les associations
	 * @return Une liste de Lieu correspondants au tag association
	 */
	public List<Lieu> getAssociation() {
		List<Lieu> list = new ArrayList<>();
		try {
			//Vie etu (assoc)
			XPathExpression expr = xpath.compile("/osm/way/tag[@k='association']/parent::way/@id");
			NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			for(int i = 0; i< nodes.getLength();i++){
				long id = Long.parseLong(nodes.item(i).getNodeValue());
				Lieu l = new Lieu(id);
				l.setAttribut("association");
				list.add(l);
			}
			for(int i = 0; i< nodes.getLength();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				Long idG = Long.parseLong(this.xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='association']/parent::way/*[1]/@ref", doc));
				list.get(i).setIdG(idG);
			}
			for(int i = 0; i< list.size();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				String nom = xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='association']/@v", doc);
				list.get(i).setNom(nom);
			}
			for(int i = 0; i< list.size();i++){
				this.setLatAndLon(list.get(i),doc,xpath);
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return list;
	}

	//COURS
	/**
	 * Fonction pour avoir les amphitheatres
	 * @return Une liste de Lieu correspondants au tag amphi
	 */
	public List<Lieu> getAmphis() {
		List<Lieu> list = new ArrayList<>();
		try {
			//Amphi
			XPathExpression expr = xpath.compile("/osm/way/tag[@k='amphi']/parent::way/@id");
			NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			for(int i = 0; i< nodes.getLength();i++){
				long id = Long.parseLong(nodes.item(i).getNodeValue());
				Lieu l = new Lieu(id);
				l.setAttribut("amphi");
				String spid = String.valueOf(id);
				long idG = Long.parseLong(this.xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='amphi']/parent::way/*[1]/@ref", doc));
				l.setIdG(idG);
				String nom = xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='amphi']/@v", doc);
				l.setNom(nom);
				this.setLatAndLon(l,doc,xpath);
				list.add(l);
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return list;
	}

	//ADMINISTRATION
	/**
	 * Fonction pour avoir les secretariats
	 * @return Une liste de Lieu correspondants au tag secretariat
	 */
	public List<Lieu> getSecretariat() {
		List<Lieu> list = new ArrayList<>();
		try {
			//secreteriat
			XPathExpression expr = xpath.compile("/osm/way/tag[@k='secretariat']/parent::way/@id");
			NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			for(int i = 0; i< nodes.getLength();i++){
				long id = Long.parseLong(nodes.item(i).getNodeValue());
				Lieu l = new Lieu(id);
				l.setAttribut("secretariat");
				list.add(l);
			}
			for(int i = 0; i< nodes.getLength();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				long idG = Long.parseLong(this.xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='secretariat']/parent::way/*[1]/@ref", doc));
				list.get(i).setIdG(idG);
			}
			for(int i = 0; i< list.size();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				String nom = xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='secretariat']/@v", doc);
				list.get(i).setNom(nom);
			}
			for(int i = 0; i< list.size();i++){
				this.setLatAndLon(list.get(i),doc,xpath);
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Fonction pour avoir les secreteriats (autres nomenclature)
	 * @return Une liste de Lieu correspondants au tag secretariatBis
	 */
	public List<Lieu> getSecretariatBis() {
		List<Lieu> list = new ArrayList<>();
		try {
			//secreteriat
			XPathExpression expr = xpath.compile("/osm/way/tag[@k='secretariatBis']/parent::way/@id");
			NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			for(int i = 0; i< nodes.getLength();i++){
				long id = Long.parseLong(nodes.item(i).getNodeValue());
				Lieu l = new Lieu(id);
				l.setAttribut("secretariat");
				list.add(l);
			}
			for(int i = 0; i< nodes.getLength();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				long idG = Long.parseLong(this.xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='secretariatBis']/parent::way/*[1]/@ref", doc));
				list.get(i).setIdG(idG);
			}
			for(int i = 0; i< list.size();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				String nom = xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='secretariatBis']/@v", doc);
				list.get(i).setNom(nom);
			}
			for(int i = 0; i< list.size();i++){
				this.setLatAndLon(list.get(i),doc,xpath);
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return list;
	}

	//AUTRE
	/**
	 * Fonction pour avoir les laboratoires
	 * @return Une liste de Lieu correspondants au tag batRecherche
	 */
	public List<Lieu> getBatRecherche() {
		List<Lieu> list = new ArrayList<>();
		try {
			//bat recherche
			XPathExpression expr = xpath.compile("/osm/way/tag[@k='batRecherche']/parent::way/@id");
			NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			for(int i = 0; i< nodes.getLength();i++){
				long id = Long.parseLong(nodes.item(i).getNodeValue());
				Lieu l = new Lieu(id);
				l.setAttribut("batiment recherche");
				list.add(l);
			}
			for(int i = 0; i< nodes.getLength();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				long idG = Long.parseLong(this.xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='batRecherche']/parent::way/*[1]/@ref", doc));
				list.get(i).setIdG(idG);
			}
			for(int i = 0; i< list.size();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				String nom = xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='batRecherche']/@v", doc);
				list.get(i).setNom(nom);
			}
			for(int i = 0; i< list.size();i++){
				this.setLatAndLon(list.get(i),doc,xpath);
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Fonction pour avoir les batiments ne rentrant dans aucune particuliere
	 * @return Une liste de Lieu correspondants au tag nameBis
	 */
	public List<Lieu> getAutres() {
		List<Lieu> list = new ArrayList<>();
		try {
			//autre
			XPathExpression expr = xpath.compile("/osm/way/tag[@k='nameBis']/parent::way/@id");
			NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			for(int i = 0; i< nodes.getLength();i++){
				long id = Long.parseLong(nodes.item(i).getNodeValue());
				Lieu l = new Lieu(id);
				l.setAttribut("autre batiment recherche");
				list.add(l);
			}
			for(int i = 0; i< nodes.getLength();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				long idG = Long.parseLong(this.xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='nameBis']/parent::way/*[1]/@ref", doc));
				list.get(i).setIdG(idG);
			}
			for(int i = 0; i< list.size();i++){
				long pid = list.get(i).getId();
				String spid = String.valueOf(pid);
				String nom = xpath.evaluate("/osm/way[@id="+spid+"]/tag[@k='nameBis']/@v", doc);
				list.get(i).setNom(nom);
			}
			for(int i = 0; i< list.size();i++){
				this.setLatAndLon(list.get(i),doc,xpath);
			}

		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return list;
	}

	//Fonction pour affecter la longitude et la latitude

	/**
	 *
	 * @param l Le Lieu dont on veut affecter les coordonnees
	 * @param doc le document ou sont stockees les informations xml (en general le fichier osm) (voir attribut de la classe XMLReader)
	 * @param xpath l'attribut xpath pour executer les requetes xpath (voir attribut de la classe XMLReader)
	 */
	public void setLatAndLon(Lieu l, Document doc, XPath xpath){
		try {
			String id = String.valueOf(l.getIdG());
			String s = xpath.evaluate("/osm/node[@id="+id+"]/@lat", doc);
			float lat = Float.parseFloat(s);
			l.setLatitude(lat);
			s = xpath.evaluate("/osm/node[@id="+id+"]/@lon", doc);
			float lon = Float.parseFloat(s);
			l.setLongitude(lon);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Une fonction d'affichage (comme le toString mais uniquement pour le nom)
	 * @param lLieu La liste de Lieu dont on veut afficher les noms
	 * @return Une liste de String contenant tous les noms
	 */
	public String[] getAllName(List<Lieu> lLieu){
		String nomsConc = "";
		for(Lieu lieu : lLieu)
			nomsConc+=lieu.getNom()+",";
		String[] allName = nomsConc.split(",");
		return  allName;
	}
}
